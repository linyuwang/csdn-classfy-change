import urllib.request

import re
patterm = "<span title=\"单击删除该标签\">[\w]+</span>"
patterm = re.compile(patterm)
articles = []
#读取原来文章的tag和分类，并且处理成数组
with open("classify_lable.txt","r",encoding="utf-8") as f:
	for i in f:
		i = i.split("###,###")
		i[2] = patterm.findall(i[2])
		k = ""
		j = 0
		while j < len(i[2]):
			i[2][j] = i[2][j].replace("<span title=\"单击删除该标签\">","")
			i[2][j] = i[2][j].replace("</span>","")
			k+=" "+i[2][j]
			j+=1

		articles.append(i)
print("articles: "+str(len(articles)))

#读取原来的分类列表，并且映射成字典，方便下一步
origin = {}
now = {}
with open("classfyMap.txt","r",encoding="utf-8") as f:
	for i in f:
		i = i.replace("\n","")
		i = i.split("###,###")
		if(len(i[1]) == 0):
			continue
		i[1] = i[1].split(",")
		origin[i[0]] = i[1]
		for j in i[1]:
			now[j] = 1
print("old category: %d"%(len(origin)))
print("now category: %d"%(len(now)))
# for i in now.keys():
# 	print(i)
#对于每一篇文章，更新tag以及把旧的类别换成新的，新的类别以'##'开头
#这里用字典可以去重，tag和类别都不会重复
new_article = []
nolable = 0
nocategory = 0
for i in articles:
	lable_c = {}
	new_c = {}
	for j in i[2]:
		if(len(j) > 0):
			lable_c[j] = 1
	for j in i[1].split(","):
		if(len(j) > 0):
			lable_c[j] = 1
		if j in origin:
			for k in origin[j]:
				new_c[k] = 1
	category = []
	lable = []
	for j in lable_c.keys():
		lable.append(j)
	if(len(new_c) == 0):
		for j in lable:
			if(j in origin):
				new_c[j] = 1
	for j in new_c.keys():
		category.append(j)
	if(len(lable) == 0):
		nolable += 1
	if(len(category) == 0):
		nocategory += 1
	new_article.append([i[0],lable,category])
print("finale set: %d nolable: %d nocategory: %d"%(len(new_article),nolable,nocategory))
# for i in new_article:
# 	print(i)
#输出成js数组，用于下一步操作
js_arry = "var arti = ["
m = 0
for i in new_article:
	lab = ""
	cat = ""
	for j in i[1]:
		lab += ("<span title=\'单击删除该标签\'>%s</span>"%(j))
	k = 0
	while k < len(i[2]):
		if k > 0:
			cat +=','
		cat += "##"+i[2][k]
		k += 1
	if (m > 0):
		js_arry += ",\n"
	js_arry += '["%s","%s","%s"]'%(i[0],lab,cat)


	m+=1
js_arry +="];\n"
print(js_arry)








